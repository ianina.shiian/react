export const toTwoDigitTimeFormat = time => {
  return String(time).padStart(2, '0');
};
