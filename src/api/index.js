class Api {
  getList = () => {
    fetch('http://localhost:8000/list', {
      method: 'GET'
    }).then(data => data.json());
  };
}

export default new Api();
