import React, { Component } from 'react';
import { func, bool, object } from 'prop-types'; // ES6
import cx from 'classnames';

import { toTwoDigitTimeFormat } from '../../utils/toTwoDigitTimeFormat';
import { ALARM_TIME, TIMER_DURATION } from './constants';
import './styles.css';

class Timer extends Component {
  state = {
    minutes: 0,
    seconds: 0,
    isRun: false,
    isOver: false,
    alarmTime: false
  };

  constructor(props) {
    super(props);
    const { time } = props;

    if (time) {
      const { minutes, seconds } = time;

      // eslint-disable-next-line
      this.state.minutes = minutes;
      // eslint-disable-next-line
      this.state.seconds = seconds;
    }

    this.audio = new Audio('./system-fault.mp3');
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  increment = () => {
    let { minutes } = this.state;

    minutes = minutes === TIMER_DURATION ? 0 : minutes + 1;
    this.setState({ minutes });
  };

  decrement = () => {
    let { minutes } = this.state;

    minutes = minutes === 0 ? TIMER_DURATION : minutes - 1;
    this.setState({ minutes });
  };

  start = () => {
    this.setState({ isRun: true });
    this.interval = setInterval(this.tick, 1000);
  };

  pause = () => {
    clearInterval(this.interval);
    this.setState({ isRun: false });
  };

  tick = () => {
    let { minutes, seconds } = this.state;

    if (minutes === 0 && seconds === 0) {
      clearInterval(this.interval);

      this.setState({
        isRun: false,
        isOver: true
      });
      this.audio.play();
      this.props.callback();

      return;
    }

    if (seconds === 0) {
      seconds = 60;
      minutes -= 1;
    }

    if (seconds <= ALARM_TIME && minutes === 0) {
      this.setState({ alarmTime: true });
    }

    this.setState({ seconds: seconds - 1, minutes });
  };

  reset = () => {
    clearInterval(this.interval);

    this.setState({
      minutes: 0,
      seconds: 0,
      isRun: false,
      isOver: false,
      alarmTime: false
    });
  };

  get alarmBackground() {
    const {
      isRun,
      isOver,
      alarmTime
    } = this.state;

    return !isRun && isOver && alarmTime;
  }

  render() {
    const { controls } = this.props;
    const {
      minutes,
      seconds,
      isRun,
      isOver,
      alarmTime
    } = this.state;

    return (
      <div className={cx('timer', { 'timer--mark': this.alarmBackground })}>
        <h2>Timer</h2>

        <div className="timer__box">
          <div className="timer__setter">
            {controls &&
              !isRun && [
                <span className="timer__arm timer__arm--up" key="up" onClick={this.increment}>
                  &#9650;
                </span>,
                <span className="timer__arm timer__arm--down" key="down" onClick={this.decrement}>
                  &#9660;
                </span>
              ]}
            <span className="timer__item">{toTwoDigitTimeFormat(minutes)}</span>
          </div>
          <span className="timer__item">:</span>
          <span className={cx('timer__item', { 'timer__item--mark': alarmTime })}>{toTwoDigitTimeFormat(seconds)}</span>
        </div>

        {controls && (
          <React.Fragment>
            {!isRun && !isOver && <button className="timer__button" onClick={this.start}>Start</button>}
            {isRun && !isOver && <button className="timer__button" onClick={this.pause}>Pause</button>}
            {isOver && <button className="timer__button" onClick={this.reset}>Reset</button>}
          </React.Fragment>
        )}
      </div>
    );
  }
}

Timer.propTypes = {
  callback: func,
  time: object,
  controls: bool
};

Timer.defaultProps = {
  callback: () => null
};

export default Timer;
