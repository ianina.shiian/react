import React, { Component } from 'react';
import api from '../../api';
import ListItem from '../List/list.item.component';
import './style.css';

class List extends Component {
  state = {
    list: [],
    loading: false,
    selectedItem: ''
  };

  componentDidMount() {
    this.setState({ loading: true });
    api.getList().then(data => {
      this.setState({ loading: false, list: data });
    });
  }

  onClick = name => {
    this.setState({ selectedItem: name });
  };

  render() {
    const { data, loading, selectedItem } = this.state;

    return (
      <div className="aside">
        <h2>Downloaded files</h2>

        <ol className="list">
          {loading ? (
            <h3>Loading...</h3>
          ) : (
            data.map(name => (
              <ListItem name={name} key={name} onClick={this.onClick} selected={selectedItem === name}></ListItem>
            ))
          )}
        </ol>
      </div>
    );
  }
}

export default List;
