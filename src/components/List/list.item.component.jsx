import React from 'react';

function ListItem(props) {
  const { name, onClick, selected } = props;
  const itemStyle = selected ? 'listItem listItemSelected' : 'listItem';

  return (
    <li className={itemStyle} onClick={() => onClick(name)}>
      {name}
    </li>
  );
}

export default ListItem;
