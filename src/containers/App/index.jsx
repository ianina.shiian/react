import React, { Component } from 'react';
// import List from '../../components/List';
import Timer from '../../components/Timer';
import './styles.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.timer = React.createRef();
  }

  timerStart = () => this.timer.current.start();

  timerPause = () => this.timer.current.pause();

  timerReset = () => this.timer.current.reset();

  render() {
    return (
      <div className="App">
        {/* <List /> */}

        <div className="btn-controls">
          <button className="btn-controls__item" onClick={this.timerStart}>Start timer</button>
          <button className="btn-controls__item" onClick={this.timerPause}>Pause timer</button>
          <button className="btn-controls__item" onClick={this.timerReset}>Reset timer</button>
        </div>

        <Timer
          callback={() => console.log('finish')}
          controls
          time={{ minutes: 0, seconds: 13 }}
          ref={this.timer} />
      </div>
    );
  }
}

export default App;
