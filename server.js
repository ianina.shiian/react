const express = require('express');
const fileUpload = require('express-fileupload');

const app = express();
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser');

const users = [
  {
    email: 'jane@a.a',
    password: '1111',
    firstName: 'Jane',
    lastName: 'Shine'
  },
  {
    email: 'name@a.a',
    password: '1111',
    firstName: 'Name1',
    lastName: 'Name2'
  }
];

app.use('/form', express.static(path.join(__dirname, '/public/index.html')));
app.use('/styles', express.static(path.join(__dirname, '/public/style.css')));

app.use('/app.bundle.js', express.static(path.join(__dirname, '/public/app.bundle.js')));

app.use('/nature.jpg', express.static(path.join(__dirname, 'assets/nature.jpg')));
app.use('/files', express.static(path.join(__dirname, '/uploads')));
app.use('/favicon.ico', express.static(path.join(__dirname, '/assets/favicon.ico')));

app.use(fileUpload());

app.use(bodyParser.json());

app.post('/ping', function(req, res) {
  res.send('pong');
});

app.post('/login', function(req, res) {
  const { email, password } = req.body;
  const user = users.filter(user => user.email === email && user.password === password)[0];

  if (user === undefined) {
    res.status(401).send('Not authorized');
    return;
  }

  const { firstName, lastName } = user;
  const newUser = { firstName, lastName };
  res.send(newUser);
});

app.get('/list', function(req, res) {
  const route = path.join(__dirname, '/uploads');
  fs.readdir(route, function(err, file) {
    if (err) {
      return res.status(500).send(err);
    }

    const list = file.filter(el => el !== '.gitkeep');
    res.set({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'DELETE,GET,PATCH,POST,PUT',
      'Access-Control-Allow-Headers': 'Content-Type,Authorization'
    });
    res.send(list);
  });
});

app.post('/upload', function(req, res) {
  let sampleFile = null;
  let uploadPath = null;

  if (Object.keys(req.files).length === 0) {
    res.status(400).send('No files were uploaded.');
    return;
  }

  sampleFile = req.files.sampleFile; // eslint-disable-line

  uploadPath = path.join(__dirname, '/uploads/', sampleFile.name);

  sampleFile.mv(uploadPath, function(err) {
    if (err) {
      return res.status(500).send(err);
    }

    res.send({ name: sampleFile.name });
  });
});

app.listen(8000, function() {
  console.log('Express server listening on port 8000'); // eslint-disable-line
});
